Autobiography of Kwame Nkrumah
ot if you cannot help your country in her hour of
Need ¢

‘In spite of the humble conditions under which we have
started,’ I said, ‘I bring you a message of hope and inspiration.
I bid you shake hands with yourselves and your teachers
over your study tables and before the blackboards. I look
forward to the time when there will be a chain of Ghana
colleges in all the four territories which make up the Gold
Coast—leading up to the founding of the University of
Ghana.

“In the name of the people of the Gold Coast,’ I concluded,
“in the name of humanity and in the name of Almighty
God, I bid you go forward till we realise a free and United
Gold Coast in a United West Africa !’

Sure enough, the Ghana College thrived. A year later the
number of students had increased to two hundred and thirty
and there were over a thousand names on the waiting list.
Following on the success of this college I later founded over
a dozen Ghana national secondary schools and colleges
throughout the country.

But the Working Committee were not at all grateful for
what I had achieved in the name of the Convention. They
continued to complain bitterly about the whole thing.

Still nursing their grievances, they suggested to me that
I should resign my post as general secretary and that I should
be given a hundred pounds to buy a one-way ticket to the
United Kingdom and carry on there where I left off.

I challenged this and once more denied their charges
which I told them I considered to be cooked up. It had
some effect, and certain members of the Committee began
to waver. They feared that my removal from the U.G.C.C.
might cause the complete break-down of the movement.
They could not blind themselves to the fact that I had a
strong personal following and they were beginning to consider
me as a key man in the survival of the movement. They
therefore proposed that instead of my removal, I should be
offered the post of treasurer. I accepted this for it suited my
purpose well enough, and, more important, it completely
defeated the purpose of the Working Committee. . As far

92
