 

Plate 2 (upper) The ferry across the Ankobra River which has replaced the
canoes of my childhood. (lower) Revisiting the pool at Nkroful which once
seemed to me as large and as frightening as the Atlantic Ocean
