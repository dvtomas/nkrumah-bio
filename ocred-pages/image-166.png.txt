Tactical Action

between them and the government machine runs reasonably
smoothly. |

But I must not leave the impression that everything was
plain sailing. Apart from those who succeeded in making the
best of things and discovered before long that their heart
was really in the cause, there were others who, instead of
leaving the country as their conscience dictated, chose for
one reason or another to stay and battle on, making life
miserable for themselves and objectionable for those with
whom they came in contact. For instance it did not escape
my notice that where the administrative service was con-
cerned, ifa policy was laid down for the officials by the Govern-
ment with which they disagreed, means were adopted, by
subterfuge or otherwise, to wreck that policy. At other times
I would find that matters that I wanted to be dealt with
urgently, would be delayed indefinitely (because they were
not approved of by some of the officials) until I had to
intervene and get the job done.

In certain cases, although it was never easy to obtain
evidence to prove it, it was impossible to believe that the
civil service was as unbiased politically as it was supposed to
be. It happened too often for it to be a coincidence that
whenever government policy was to be put into effect, the
officials either dilly-dallied'or saw that nothing was done about
it. Again, I could at one time almost guarantee that if there
was any movement afoot against the Government, every
attempt was made on the part of the civil service to enhance
the opposition against the Government. Whether this was
an example of the inherent sympathy of the British for the
underdog is not clear, but it is liable to breed distrust.

Certain difficulties arose also with regard to the Chief
Commissioners and the District Commissioners out in the
field. Before my Party came into office the Chief Com-
missioners and their satellites, the District Commissioners,
were actually the little governors of the rural areas over
which they exercised their control. I knew by travelling
through the country that many of these officers were hated
by the people and that, because they represented colonialism
and imperialism in practice, their presence was harmful to the

I5I
