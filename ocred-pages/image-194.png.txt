Prime Minister and Constitutional Reform

This suggestion was received enthusiastically. I felt that
unless the pot was kept constantly stirred, the contents would
go stale. I knew from experience how long it took to get
anything done, especially when the ideas involved were some-
what revolutionary, but if a constant agitation were kept up
I could not see how we would fail to reach the goal of
independence.

179

 
