Chapter 15

PRIME MINISTER AND CONSTITUTIONAL
REFORM

In the role of leader of a majority party in the Assembly which
had been elected under a ‘ bogus and fraudulent ’ constitution
that in no way satisfied the political aspirations of my Party,
my task was not an enviable one.. The Coussey Constitution
had not been formulated with the idea of a party system
developing in the country. The post of Leader of Government
Business had not been planned with a view to Africans taking
over the Government and, in fact, by winning the 1951
Election with such a majority and by forming the Government
the position of Leader of Government Business had already
become redundant. Even The Times, as early as 17th February,
1951, pointed out the need for the creation of the post of
Prime Minister, when, commenting on the title of Leader of
Government Business, it observed that under the Coussey
Constitution ‘ There is no provision for a Prime Minister. That
dignity exists only when the highest Ministerial office is com-
bined with the leadership of the predominant party in the
legislature.’

Party members insisted strongly that my title should be
changed to that of Prime Minister. There was much criticism
about it as the general feeling was that merely re-naming the
present post of Leader of Government Business to Prime
Minister would not change the real substance of the constitu-
tion. I maintained, however, that there was something in
a name, that as Prime Minister it would be necessary to
abide by the conventions that go to make a prime minister
and that since my position in the Assembly as the leader of
the Government was consequent upon leadership of the
majority party, the post of Prime Minister was definitely
called for.

Eventually we won the day. On the afternoon of sth

169
