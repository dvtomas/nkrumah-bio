Commissions of Enquiry

time by various Commissions, referred to the question of the
indebtedness of farmers in this country. Professor Shephard
in a Report which he made as carly as 1938 recommended
strongly the establishment of an organisation of farmers, both
for the marketing of cocoa and for the issuing of loans to
meet the indebtedness of farmers. Under Gold Coast condi-
tions, however, it seemed clear to me that such loans would
have to be issued through organisations rather than direct to
farmers, for in this country it is generally difficult to secure
title to property which would be acceptable as a basis for
ranting loans. If loans were given through organisations,
I felt that such organisations—having some control over their
members—would be better equipped to ensure redemption
of loans than those who made loans direct to individual
farmers. With this in view, the Cocoa Purchasing Company
was set up as a subsidiary of the Cocoa Marketing Board.

Within a year of the formation of the Cocoa Purchasing
Company thousands of farms had been redeemed by the
original owners and within three years this Company had
become the primary local cocoa buying agent in the country
—and this in face of severe competition from foreign firms
long established in the cocoa trade in the Gold Coast.

Before long, however, rumours began to spread concern-
ing the Company. In 1953 the then Chairman of the Cocoa
Marketing Board made a report to the Board pointing out
some irregularities in the C.P.C. When I received a copy of
this report, I discussed it with the Central Committee of my
Party and I also referred the matter to the Minister responsible
for the broad policy of the Cocoa Marketing Board. When
this Board appeared to do nothing to implement the report
of its Chairman, however, I decided that it was necessary for
me to see what influence I could exert on the Cocoa Purchasing
Company (through the Cocoa Marketing Board) to put its
house in order. I could not stand by and watch something
that I had created and which had flourished so rapidly suddenly
wither away through the machinations of people who sought
its destruction.

Towards the end of 1955 I ordered an enquiry by the
police into the affairs of the Company. After this, petition

255
