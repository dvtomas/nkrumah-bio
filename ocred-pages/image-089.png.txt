Autobiography of Kwame Nkrumah

started raising funds in one way and another for the organisa-
tion so that in a short while I was able to open a banking
account on behalf of the Working Committee.

The organisation and building up of a political movement
in the Gold Coast at that time was no easy task. It called for
much intensive travelling and the roads in many cases were
just bumpy tracks. Things might have been a bit better if
I had had a more reliable car, but the ancient model that
was supplied to me rarely finished a journey. It usually meant
that I had to leave it with the driver while I continued my
trek on foot. Sometimes if a ‘mammy’ lorry happened to
be rattling past I managed to get a lift, but I was not always
lucky in this respect. Most times I managed to arrange things
so that I spent the night in one of the villages but on several
occasions, when I had got stuck too far out in the bush by
nightfall, I was obliged to sleep on the roadside.

During those days all my worldly goods—two suits, two
pairs of shoes and a few underclothes—could be easily stored
in one small suitcase. This considerably facilitated my move-
ments, especially as most of the time I had to carry my case
myself. I travelled extensively to every corner of the country,
holding rallies, making contacts and delivering hundreds of
speeches.

I was not surprised to find a feeling of discontent and
unrest among the people for, while I was editing The New
African in London I came across many African soldiers who
had served during the war in Burma and the Middle East.
They had become fully conscious of their inferior standard of
living and when they returned to the Gold Coast they lost
no time in seeking better conditions. This, together with a
feeling of frustration among the educated Africans who
saw no way of ever experiencing political power under
the existing colonial regime, made fertile ground for nationalist
agitation. It was consciousness of political and economic
hardships and the social unrest after the war that led to the
crisis in February and March, 1948.

It was while I was staying with my mother at Tarkwa that
the news reached me that a certain sub-chief of the Ga State,
Nii Kwabena Bonne, had organised a country-wide boycott

74
