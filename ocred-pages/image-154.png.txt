Leader of Government Business

that, during my discussions with the Governor, I had agreed
that since the C.P.P. had won a majority only in the elected
seats of the Assembly, to select my ministers solely from the
ranks of the Party might bring me into conflict with the
territorial members and with other independent members of
the Assembly. I proposed to the central committee, therefore,
that of the seven ministers, five should be party members,
one should come from the Northern Territories and the
other one from Ashanti. At first the central committee
objected to this proposal as they felt, and understandably so,
that all the ministers should be chosen from the ranks of the
party supporters. After a long discussion, however, they
accepted my suggestion and I then submitted the names to
the Governor. The five ministers belonging to my Party
were :

Kojo Botsio—Minister of Education and Social Welfare

K. A. Gbedemah—Minister of Health and Labour

A. Casely-Hayford—Minister of Agriculture and Natural
Resources

T. Hutton-Mills—Minister of Commerce, Industry and
Mines

Dr Ansah Koi—Minister of Communications and Works

I nominated as Minister of Local Government, E. O.
Asafu-Adjaye, as a representative of Ashanti, and finally, to
represent the Northern Territories, I selected Kabachewura
J. A. Braimah, as Minister without Portfolio. Under the
constitution, as leader of the majority party in the Assembly
I became the Leader of Government Business.

For the C.P.P. to win such a sweeping victory had been
unexpected and must have been an unpleasant surprise for
quite a number of people. The post of Leader of Government
Business, for instance, had been earmarked for the Attorney-
General, a post held by an overseas officer. Many other
members of the Assembly, who were opposed to the Conven-
tion People’s Party, were expecting ministerial appointments
and had made extravagant preparations in anticipation of this,
believing that if the Governor was going to nominate members
to the Executive Council under the Coussey Constitution,

139
