The Birth of My Party

tension, has decided to transform itself into a fully-fledged
political party with the object of promoting the fight for full
self-government now.”

Then, on behalf of the C.Y.O., in the name of the chiefs,
the people, the rank and file of the Convention, the Labour
Movement, our valiant ex-servicemen, the youth movement
throughout the country, the man in the street, our children
and those yet unborn, the new Ghana that is to be, Sergeant
Adjety and his comrades who died at the crossroads of
Christiansborg during the 1948 riot, and in the name of God
Almighty and humanity, I declared to the crowd the birth
of the Convention People’s Party which would, from that
day forward, carry on the struggle for the liberation of our
dear Ghana in the recognised party system, until full self-
government was won for the chiefs and people of the country.

The applause which had been tumultuous eventually died
away and a deep silence followed. It was a most touching
moment for each one of us there. We had decided to take
our future into our own hands and I am sure that in those
few minutes everyone became suddenly conscious of the
burden we had undertaken. But in the faces before me I
could see no regret or doubt, only resolution.

The following Thursday, 16th June, the Working
Committee of the U.G.C.C. and the remnants of their follow-
ing held a meeting at the Palladium. Mr Obetsebi Lamptey,
when referring to me in his speech, used the word ‘ stranger’
and declared to the gathering that he failed to understand why
the Ga people, who form the majority of the population of
Accra, should permit themselves to be led by a man who did
not even come from their area, This was too much for those
who happened to be present and the meeting ended in uproar
and confusion against him. The foundations of age-old
tribalism had suffered their first irreparable crack.

When the Working Committee braved themselves to face
the fact that the C.Y.O. had actually transformed itself into
an independent political party with a definite policy and a
programme opposed to their own, a few of the right wing
members among them showed concern. They invited a
barrister from Sekondi, a minister from the Methodist Mission

105
