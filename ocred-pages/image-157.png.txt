Autobiography of Kwame Nkrumah

was to sweep away all colonial laws which restricted freedom
of speech of the press and in public assembly. In particular,
I quoted the need for amendments to the laws concerning
trade unions, the law of sedition and others which were aimed
at hindering the progress of the people. Moreover a citizen
should be tried by a jury in all criminal cases instead of the
questionable trial by assessors.

It was felt that had we not accepted office by virtue of
our majority in the Assembly, but had embarked on non-
co-operation and remained in the Opposition, we would merely
have been pursuing a negative course of action. It was more-
over the opinion of the Party Executive that by taking part
in the new government, we were at least preventing the
“stooges and reactionaries’ from taking advantage of the
position. Governmental positions could also help us to obtain
the initiative in the continuing struggle for full selfgovern-
ment. If we were obstructed in our purpose by British
bureaucratic officialdom, we did not rule out the possibility
of further Positive Action.

There should be no fraternisation, I said, between
Assemblymen and European officials except on strictly official
relations: for what imperialists failed to achieve by strong-
arm methods, they might hope to bring off with cocktail
parties. I emphasised, however, that we were not fighting
against race or colour, but against a system. To prove that
we had not led this struggle for any personal aggrandisement,
we had agreed that none of our party members in the Executive
Council should go to live in the palatial ministerial bungalows
that had been built for the purpose.

Moreover, I continued, until such time as full self-govern-
ment had been attained, I was strongly of the opinion that all
party members of the Assembly, as well as ministers, should
surrender their salaries to the Party and draw instead agreed
remuneration from party funds. This would prevent careerism
and induce those in high office to live simply and modestly
and so to maintain contact with the common people.

By launching Positive Action the previous year, we had
demonstrated to the people that we stood by our word
whatever the cost might be. ‘ The spirit of Positive Action ,

142
