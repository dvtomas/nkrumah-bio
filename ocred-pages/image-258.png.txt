The ‘ Federation’ Issue

On 26th September, therefore, Sir Frederick Bourne
arrived in the country for this purpose, and all parties and
organisations concerned were invited to avail themselves of
his services. Sit Frederick had had many years experience of
constitutional matters in India and Pakistan and at first,
because of his connection with Pakistan, some people were
a little worried as to whether he would not advocate a
partition of the Gold Coast. But they need have had no
fears, as it turned out, for he was not only highly intelligent
but conscientious and sincere in the mission he had under-
taken. He also acquired, after a short while in the country,
a true affection for and understanding of the people whose
cause he took very deeply to heart. He visited all the regions
and met and discussed with a great number of bodies and
individuals. When he went to Kumasi, however, to meet
the leaders of the N.L.M. and its allies, they preferred not
to see him, making as their excuse that the Government, by
passing the State Councils (Ashanti) Amendment Bill had
prejudiced his work and had shown that they were neither
sincere nor serious in their attitude to him,’ Because of the
passing of that Bill, the Opposition were unwilling to take
part in any discussions regarding future constitutional develop-
ments.

Sir Frederick, along with many others, felt that the State
Councils Ordinance was ill-timed and that it should have
been postponed until a later date. But it should be appreciated
that the circumstances in the country at that particular time
warranted passing into law an ordinance of that nature, when
chiefs were being unjustly destooled for not supporting the
federalist idea. They had no right of appeal and it was high
time that the Government took measures to protect them
from such unfair treatment. If the Bill had been withdrawn,
it would have weakened the Assembly, and the chiefs for whose
benefit it was being put through would have suffered at the
hands of those who dealt in corruption, violence and injustice.

The Constitutional Adviser completed his work and
reported to the Governor on 17th December. In this Report
he recommended the devolution of certain powers and
functions to Regional Assemblies which would be empowered

243
