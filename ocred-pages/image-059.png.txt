 

Autobiography of Kwame Nkrumah

other movements in West Africa. We believed that unless
territorial freedom was ultimately linked up with the Pan
African movement for the liberation of the whole African
continent, there would be no hope of freedom and equality
for the African and for people of African descent in any part
of the world.

The idea of West African unity, which, of course, I strongly
supported, became the accepted philosophy of the African
Students’ Association and we directed the students that when
they returned to their respective territories they should work
hard politically to organise particular areas, but that in so
doing they should maintain close contact with the political
activities of their territories. By this means they would
maintain not only unity within their own territories, but
would pave the way for unity among all the territories in
West Africa.

At that time I was interested in two sociological schools
of thought in the States, one represented by the Howard
Sociologists led by Professor Fraser, and the other led by
Dr M, J. Herzkovits, Professor of Anthropology at North-
western University. The Howard school of thought main-
tained that the Negro in America had completely lost his
cultural contact with Africa and the other school, represented
by Herzkovits, maintained that there were still African
survivals in the United States and that the Negro of America
had in no way lost his cultural contact with the African

continent. I supported, and still support, the latter view and
1 went on one occasion to Howard University to defend it.

Apart from all my academic work and the various activities
that I have mentioned, I made time to acquaint myself with
as many political organisations in the United States as I could.

These included the Republicans, the Democrats, the Com-
munists and the Trotskyites.

It was in connection with the
last movement that I met one of its leading members
Mr C. L. R. James, and through him I learned how an under-.
ground movement worked.

I was also brought i
: oe into con
with organisations such as the Council on a a

5 i tican Affaj
the Committee on Africa, the Committee on War and a
Aims, the Committee on African Students, the § ae ‘
a
4 P
