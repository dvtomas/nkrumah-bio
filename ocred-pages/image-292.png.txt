The Final Test

known and it was my view that there was nothing to prevent
us now fulfilling the final conditions of the Secretary of
State’s statement. There was also no reason why the Secretary
= sere in his turn, should feel any reluctance to fulfil his
pledge.

I then dealt in detail with the Opposition’s desire to
partition the country into a number of small autonomous
regions. ‘Such a prospect,’ I said, ‘in the conditions of the
modern world, is too unreal to contemplate.’ In any event
this case had been put to the country in the recent election
and had been rejected.

“There is no shame in electoral defeat,’ I continued,
“shame lies only in being a bad loser.’ And I added that to
allow the minority to override the decision of the majority
would be to make a farce of parliamentary government. I
had always been ready to recognise, I went on, as had been
put forward in the Government’s constitutional proposals
last May, that there should be a measure of devolution of
powers from the central Government to the Regions. I
confirmed that the proposals contained in Sir Frederick
Bourne’s Report on the subject, as modified by the Achimota
Conference, were generally acceptable to the Government.
* But,’ I emphasised, ‘ the Government considers that it would
be impracticable, and indeed, dangerous, for this country to
try to proceed further than that along the road to federalism.’

I went on to point out to the House the immense burden
of administration that Federation would impose on our small
country and to consider whether, with our present manifold
problems, we could shoulder such an increased burden.
“How are we to finance such a lavish structure of govern-
ment?’ I asked. ‘Are we to sacrifice the welfare of the
masses of our people to multiple government for its own
sake? Are our people, only now beginning to struggle
painfully out of dismal want, to be pushed back to support an
administrative structure so cumbersome and eating so heavily
into our financial resources that there would be nothing left
for it to administer?’ I repeated what I had said in the
House in August of the previous year, that my determina-
tion was that we should attain independence with as

277
