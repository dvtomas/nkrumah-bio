The ‘ Federation’ Issue

inside or outside Parliament condemning the violence being
perpetrated in Ashanti; in fact nothing has encouraged the
N.L.M. more than that statement he made saying that there
would have to be a ‘ general agreement ’, whatever that might
mean, before self-government was granted. You might also
add that I was very surprised that no apology was demanded
from the N.L.M. when they stoned the Governor’s car in
Kumasi.’

I then detailed my plans, to present my White Paper at
the forthcoming Budget session of the Assembly, and a second
White Paper indicating the amendments to be made to our
existing Constitutional Instruments to enable the Gold Coast
to become a sovereign and independent State within the
Commonwealth, and, after a debate on these, to make a formal
motion for independence. I would then expect the British
Government to proceed with the enactment of the necessary
legislation to grant us our independence. I stressed the ill-
effects that the uncertainty of this transitional period was
having on the country and ended by saying that it was my
firm resolve not to weaken the cause of democracy in the
Gold Coast by setting at naught the provisions of the Con-
stitution and the powers of our Legislative Assembly.

On 23rd March Kojo Botsio took off from Accra airport.
I did not want to cause comment by seeing him off and had
to content myself with waving to the plane as it roared over
my house. It was an anxious mission for him and I knew
that he felt deeply the responsibility with which he had been
entrusted. But I don’t think I could have relied on any better
man for putting over my views.

Both the Governor and Mr Botsio returned to Accra a
week later. It was Good Friday and I had arranged to tour
the three hospitals in Accra. I had not known at the time I
made arrangements for this that Kojo would be returning on
that day, for I had had no word from him apart from a cable
to say that he had arrived safely. Anyhow, we managed to
have a short chat before I set out and he assured me that
things had gone well, though perhaps not exactly as I had
planned. He had been impressed, he said, by the sincerity of
Mr Lennox-Boyd and he felt sure that he was genuinely

251
