Hard Times

American ports where we docked. Sometimes I would join
them, just for the fun of it and because I didn’t want to be
thought of as something unusual, but after my experience in
Las Palmas, I was always a little wary of such pastimes. I
did meet a French woman, however, who seemed to be of
a rather better sort and J used to visit her every time the boat
went to Mexico. But somehow or other she got the idea
that I had promised to marry her. Perhaps it was the language
difficulty, anyhow, rather than argue the point, I said that I
would be back the following trip to stay. I had not lived
with sailors for nothing !

I was really sorry when war broke out and my sailor
days came to an end, for apart from the lighter side of it, the
pay was reasonably good and we were always assured of
three good meals a day.

On the other hand, except for the camaraderie which
existed among the crew boys, there was always a most haunt-
ing feeling of loneliness, not just being without companions,
but of being nobody’s concern. Many times as I walked in
the streets of Vera Cruz or in other foreign ports the thought
struck me that anybody could have set upon and killed me
and nobody would have missed me unduly. I don’t suppose
any steps would have been taken to discover even my identity.

I learned too, that to sleep under the stars in my native
Africa was, in spite of the raiding mosquitoes, a far happier
prospect than sleeping out in the cities of America. When
I first visited Philadelphia with a fellow student neither of us
had any money for lodgings and, as we had nowhere else
to go, we walked back to the railway station and sat on one
of the benches intending to pass the night there. We had
not reckoned with the ubiquitous American police. At about
midnight we were rudely shaken out of our doze and greeted
by a firm but not unkind voice saying, “Move on, chums,
you can’t sleep here.’

With aching limbs and eyes almost blind for want of sleep,
we felt that it was the most cruel thing that had yet been
done to us. We shuffled out of the station and wandered
into the nearby park. Luckily, unlike the London parks, the
gates were not locked against us, and we managed to find a

39
