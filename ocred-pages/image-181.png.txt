Autobiography of Kwame Nkrumah

was full, I had talks with officers of the Department of
State and other government departments, learned how the
organisation of the United States Government worked, laid
a wreath on the memorial to Abraham Lincoln, visited the
Jefferson Memorial and attended two receptions, one at the
British Embassy and the other immediately afterwards at
the State Department.

The following day I returned to New York and prepared
for my departure. In the evening the Mayor of New York
arranged a large dinner party in honour of Kojo and myself.
All the leading Negro dignitaries and officials were present
and I gave a speech on the political development of the Gold
Coast and on our hope to become self-governing in the near
future. I expressed the desire that all those who wished to
return to help develop the country would be most welcome
after independence when we would be free to open our arms
to whomsoever we pleased. I said how very happy I had
been that I was able to pay this unexpected visit to the United
States and how welcome everyone had made me feel. I
would carry back with me many happy memories.

As I sat down in my place my thoughts went back to the
day when I delivered my last sermon in America at the
Presbyterian Church in Philadelphia. My text was ‘I saw
a new Heaven and a new Earth’ and I reminded the people
of how history repeated itself. Just as in the days of the
Egyptians, so to-day God had ordained that certain among
the African race should journey westwards to equip them-
selves with knowledge and experience for the day when they
would be called upon to return to their motherland and to
use the learning they had acquired to help improve the lot
of their brethren, I impressed upon the people that they
should not be despondent or impatient, for their turn would
come any day now. ‘Be prepared,’ I warned them, “so that
you are ready when the call comes, for that time is near at
hand.’ I had not realised at that time that I would contribute
so much towards the fulfilment of this prophecy.

~ On roth June, Kojo and I waved goodbye to a crowd of

officials, students and other well-wishers who had assembled

at the airport to send us on our way, We both felt happy in
166
