In Search of Relaxation

the captain came along. ‘How’s the relaxation going ?”
he asked.

“I don’t know, captain. I’m not used to lying around
like this all day. I think you'll have to find me a job before
the trip is over.’

“Can you play darts?” he asked.

‘T’ve never tried.’

“Well come along and have a game. I always play
against the first officer before lunch. It’s my daily exercise.’

And so each noon the captain and I, with the first officer,
the Dutch Consul in Nigeria, who had been asked by the
Dutch authorities to accompany me on the trip, Daniel or
Erica, or whoever else happened to be around when we needed
a fourth player, paired up and played against each other.

“Losers buy the beer !” the captain would announce.

These gatherings round the dart board each lunchtime are
quite my happiest memories of the trip.

The following morning the boat docked at Lagos. Again
I went up to the bridge and watched the pilot come aboard
at the entrance of the harbour mouth to take the boat safely
through the tricky mud banks and shallow waters.

Soon after we docked, the A.D.C. to the Governor-General
of Nigeria and the Chief Secretary came aboard to extend
a welcome on behalf of Sir James Robertson and asked if I
would care to have tea at Government House that afternoon.
I accepted and on the return trip to Lagos I had lunch there
when I had a long and useful chat with the Governor-General
on current affairs of mutual interest. On each occasion I was
transported by the Government House launch.

Word soon got round that I was on board and people
lined the quayside day and night expecting me to come
ashore. Because so many tried to come aboard to speak to
me, a policeman had to be stationed at the top of the gangway.
I wanted very much to go ashore and say a few words to the
expectant crowds, but I realised that this might cause embarrass-
ment to the Government of Nigeria since I was on an unofficial
visit. I hoped that the people would understand and | asked that
the facts should be explained to them in the local newspapers.

But I had to find a way to visit Mr W. Biney, who had

231

muh

Be
BG
