 

Chapter 10
POSITIVE ACTION

Ir soon became apparent that the feeling of unrest was rife
not only among the rank and file of the people, but also among
the Government officials, the chiefs and the intelligentsia.
Their discontent, however, was not born of suffering but of
fear, fear of what they might have to suffer if something was
not done to check this surging nationalism, this sudden
political awakening of the youth and the man in the street.
The words ‘ positive action’ which I had used at the June
rally in Accra spelt fear in the minds of my opponents and as
soon as it became generally known that I intended leading
the Party in a campaign of ‘non-violent positive action’,
rumours reached me that plans were being made to deport
me from Accra. In fact the local Gold Coast radio announced
that I had already been banished from the capital. Shortly
after this, I received a letter asking me to appear before the
Ga State Council, the traditional local authority, in order to
discuss ‘ the unfortunate lawless elements in the country and
any possible solution’.

1 went along with two of my supporters and was surprised
to find that among those present were the ex-members of the
Working Committee of the U.G.C.C. The agenda that had
been drawn up for the meeting was never referred to, All
the speeches that were made were on one subject alone, and
the whole idea of the meeting appeared to be to censure me
for introducing the words Positive Action into the country
and thereby threatening violence.

‘ What exactly do you mean by Positive Action?’ I was
asked.

I took my time and explained to them as fully as I could
but, as I might have expected, it did not satisfy certain of
my long-standing political opponents who were present. The
Ga Manche, the paramount chief who was presiding, then
suggested that I should convene a meeting of my followers

110

 
