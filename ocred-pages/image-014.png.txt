24

25

26

27
28
29
30

31

35

36
37
38

List of Plates

The Governor asks me to form a new Government, 1954
Ceremonial opening of the 1954 Legislative Assembly
The Cabinet 1954-56

Ministers and Secretaries with the Governor after the 1956
Election

Examining a cocoa bean for marks of disease

Opening the new telephone exchange at Ho

Aboard the Nigerstroom

In the lounge of the Nigerstroom

At the helm of the Nigerstroom

Driving a railway engine

Setting off blasting operations at Tema

With Mr Adlai Stevenson

With friends and supporters in Togoland

A village welcome

Visiting a child patient

Inspecting progress of work at Tema harbour

With N.T. Chiefs in my office

Demonstrating my office telephone system

Qucue outside a polling station

Taking off in a Canberra jet bomber

My first experience of the television camera

After the results of the 1956 Election

Waving to the crowds after the announcement of Independence

Being chaired out of the Assembly after announcing the date of
Independence, 18th September 1956

Endpaper Map
Facsimile reproduction of part of an eighteenth century print
Carte de la Barbarie de la Nigritie et de ha Guinée prepared by
Guillaume Del’Isle, a member of the Academie Royale des
Sciences, and made at Amsterdam by Jean Covens and Corneille
Mortier, The print is undated.

213
213
220

220
221
221
230
231

235

274
275
286

287
