Appendix A

Its decision shall be reported to the National Executive Committee
at its next session for ratification or otherwise. Appeals lie to the Annual
Delegates’ Conference. Members of the Tribunal of Justice are appointed
annually, but members can be re-appointed.

Only the National Executive Committee can expel a member and
submit this to the Annual Delegates’ Conference for ratification. Branches,
Constituencies, Regions and the Central Committee can only suspend
defaulting members and report to the National Executive Committee for
action. . . ; t

Any member acting as candidate or supporting a candidate in opposition
to the Party’s offici candidate in any Central or Local Government
Election as duly announced shall be expelled from the Party.

REGIONAL PARTY ORGANISATION
REGIONAL CONFERENCE

A Regional Conference shall be convened annually by the Regional
Committee or on the instructions of the National Secretariat. A special
Regional Conference shall be convened by the Regional Committee on
the instructions of the National Executive Committee through the
Secretariat at National Headquarters, Accra, or on the demand of at least
one-third of the Constituencies of the Party in the Region. The Regional
Conference shall consist of two representatives from each Constituency
within the Region.

REGIONAL OFFICERS
The Regional Officers shall be as follows :
(i) Regional Chairman, elected at the Annual Conference.
(ii) Regional Vice-Chairman, elected at the Annual Conference.
(iii) Regional Treasurer, elected at the Annual Conference.
These officers are elected for one year, but can be re-elected.
(iv) Regional Propaganda Secretary.
(v) Regional Secretary.
(vi) Other Regional Officers.
These officers are appointed by the National Executive Committee
as full-time officers.
The members of the National Executive from the Region concerned
shall be ex officio delegates to the Regional Conference with full rights

Powers OF REGIONAL CONFERENCE
The Regional Conference shall have the power :
(a) To lay down Regional policy and programme for the ensuing year
providing that such policies and programmes are in conformity with

the basic policy and programme laid down by the Annual National
Delegates’ Conference.

(b) To consider the political and organisational reports and statements of

297
