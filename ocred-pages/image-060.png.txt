Hard Times

Research Council of the National Association for the Advance-
ment of Coloured People and the Urban League. My aim
was to learn the technique of organisation. I knew that when
I eventually returned to the Gold Coast I was going to be
faced with this problem. I knew that whatever the programme
for the solution of the colonial question might be, success
would depend upon the organisation adopted. I concentrated
on finding a formula by which the whole colonial question
and the problem of imperialism could be solved. I read
Hegel, Karl Marx, Engels, Lenin and Mazzini. The writings
of these men did much to influence me in my revolutionary
ideas and activities, and Karl Marx and Lenin particularly
impressed me as I felt sure that their philosophy was capable
of solving these problems. But I think that of all the literature
that I studied, the book that did more than any other to fire
my enthusiasm was Philosophy and Opinions of Marcus Garvey
published in 1923. Garvey, with his philosophy of ‘Africa
for the Africans’ and his ‘ Back to Africa’ movement, did
much to inspire the Negroes of America in the 1920's. It
is somewhat ironical that the white Americans in the South
supported Garvey in these movements of his. They did this
not because they were interested in the emancipation of the
Negro as such, but because they wanted to get rid of the
black man from the South and felt that this would solve the
Negro problem, It was unfortunate that I was never able
to meet Garvey as he had been deported from the country
before I arrived, in connection with some kind of alleged
fraud that he had got involved in. He eventually went to
England where he died in 1940.

In an attempt to put into writing some of the experiences
and philosophies I had gained from my association with the
various organisations and how these could be best employed
in dealing with the colonial question, I started writing the
pamphlet which was later entitled ‘Towards Colonial
Freedom’. Although the first draft was completed in the
United States, it was not until I was in London that I managed
to scrape up enough money to publish it. ,

In a short preface I stated my point of view as ‘an un-
compromising opposition to all colonial policies’ and put

45
