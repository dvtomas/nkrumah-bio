Autobiography of Kwame Nkrumah

coal lorries—if we were lucky enough to be first on the
scene.

But even if we had difficulty in warming our bodies
round a fire, our hearts were constantly warmed by the ever
ready offer of help by several English girls. These girls—
most of them of good class families—used to come and type
for hours on end in the evenings and they never asked a single
penny for their work. The best we could do for them was
either to put them in a taxi and pay their fare, if we happened
to be in funds or, which was more often the case, to accompany
them to the tube station and wave them goodbye. I did
one day go to the cinema with one of them, however, because
I remember as we were about to go in an Englishman made
some scathing remark about the girl being in the company
of a black man (only that was not the expression he used).
The next thing I knew was that the girl had given him a
resounding slap on the face and told him to mind his own
business and not to use such offensive language. Being the
cause of it all I suggested to her that I take her home and
leave her, but she insisted that we carried on.

We were therefore very impressed by the English women,
There was a warmth of sincerity and sympathy surrounding
them, their hearts were big and what they did for us they
did in a quiet and unassuming way and expected neither
thanks nor reward.

Within a few months of starting the West African National
Secretariat, letters began to pour in from all quarters of the
globe. As the volume of work increased I realised the
necessity of a newspaper as an official organ of the Secretariat
in order to spread some of the ideas that we were then formulat-
ing. And so I started to make plans for the publication of a
monthly paper to be called The New African. I managed to
collect fifty pounds which I deposited with the publishers
who undertook to print three thousand copies for the first
month. In March, 1946, the first issue appeared, sub-titled
“The Voice of the Awakened African’ and with the motto
“For Unity and Absolute Independence’, Within a few
hours of its appearance, the paper was completely sold out
at threepence a copy. With the able assistance of a Nigerian

56
