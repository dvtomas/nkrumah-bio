The Widening of the Gap

as the rank and file of the U.G.C.C. were concerned, the
intentions of the Working Committee appeared highly
illogical, They felt that if I was considered to be unworthy
of the job of general secretary and incapable of managing the
affairs of the Convention, how was it that I was considered
suitable to undertake the job of treasurer? And coupled
with this, the words of Danquah were still fresh in their
minds, ‘If all of us fail you, Kwame Nkrumah will never
fail you.’ They argued that if Danquah could be bold enough
to assert this at two mass rallies in Sekondi and Accra, how
then could he denounce the same Kwame Nkrumah in the
very next breath? Moreover, to the embarrassment of the
Working Committee, they made their protests manifest by
the hundreds of letters and telegrams that poured into the
office of the U.G.C.C.

The Working Committee had many headaches as far as I
was concerned and it was no doubt because they feared the
truth of certain suspicions they had about me that they
acted so hastily and made such a fiasco of their attempt to
dismiss me. For instance, they would certainly have heard
some kind of rumour about the birth of my newspaper.

For the whole time that I was general secretary of the
movement I had done my utmost to impress upon the
Working Committee the importance of establishing a news-
paper as an organ of the movement. They would not hear
of it, their excuse being that we would probably get ourselves
embroiled in sedition cases. Personally I failed to see how
any liberation movement could possibly succeed without an
effective means of broadcasting its policy to the rank and file
of the people. In the same way as I took the initiative over
the Ghana Colleges, so I made plans to launch a newspaper.

Larranged to purchase by instalments a‘ Cropper ’ printing
machine which an Accra printer allowed me to install in his
Ausco Press printing office. With the help of an assistant
editor and four boys to operate the machine, the first edition
of my paper, the Accra Evening News, appeared on the
3rd September, 1948, on the very same day that the Working
Committee had relieved me of my post as general secretary.

From the beginning, the Accra Evening News became the

93
