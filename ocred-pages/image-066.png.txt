London Days

little room of about ten feet square which I was told could
be all mine for thirty shillings a week without meals, I felt
so happy about getting anything at all that I would have
accepted it even if there had been no furniture, And this
little room became my home for the whole of my stay in
London from June 1945 until November 1947. The family
proved extremely kind and took a great interest in my well-
being. I was very rarely in before midnight as I used to
work until all hours. But whatever time I came in I always
knew that I would find something to eat left for me in the
oven. In return for all their kindness I insisted that my
landlady leave all the dirty dishes for me to wash. This I did
regularly before I went to bed.

My purpose in going to London was to study law and,
at the same time, to complete my thesis for a doctorate in
philosophy. As soon as I arrived in London, therefore, I
enrolled at Gray’s Inn and arranged to attend lectures at the
London School of Economics. It was here that I met Professor
Laski when he lectured there on political science, a subject
in which I was keenly interested. Later on I registered in
University College as well in order to read philosophy. I
abandoned the research work that I had been doing in ethno-
philosophy, which was the subject of my original thesis, and
decided instead to work on another thesis on what was then
a new theory of knowledge, ‘ Logical Positivism’. I began
this study under Professor Ayer.

In spite of this, it was only a matter of weeks before I got
myself tangled up with political activities in London,

As in my American days I associated myself with all
political movements and parties, and I felt that the Communist
Party in England was fortunate in having among its
leaders personalities such as Emil Burns, Palme Dutt and
Harry Pollitt.

The first move was to become a member of the West
African Students’ Union, of which I later became vice-
president. After much organisation, the Union became an
effective body both in taking care of new students (especially
in finding accommodation and arranging for their registration
at the various Inns of Court or Colleges) and also in agitating

51
